#nltk_data

Minimum nltk data corpora for TextBlob:

  * brown   (Required for FastNPExtractor)
  * punkt   (Required for WordTokenizer)
  * wordnet (Required for lemmatization)

Used for deployment with the heroku build pack:
https://github.com/alyssaq/heroku-buildpack-python-sklearn
